//
//  Coordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import AppkeeCoreFramework

enum CoordinatorRoutes {
    case login
    case projects(projects: [AppkeeProject])
    case openProject(code: Int)
}

protocol Coordinator: class {
    var children: Coordinator? { get set }
    var dependencies: FullDependencies { get set }
    func start()
//    func start(with routeType: DeepLinkRouteType, params: [DeepLinkParams]?)
}

protocol CoordinatorDelegate: class {
    func navigate(to: CoordinatorRoutes)
}
