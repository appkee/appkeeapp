//
//  AppCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import UIKit
import AppkeeCoreFramework

class AppCoordinator: Coordinator {
    let window: UIWindow
    let application: UIApplication
    let launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    
    var children: Coordinator?
    var appkeeChildren: AppkeeCoordinator?
    var dependencies: FullDependencies
    
    var projects: [AppkeeProject] = []
    
    // MARK: - Init
    
    init(window: UIWindow,
         application: UIApplication,
         launchOptions: [UIApplication.LaunchOptionsKey: Any]?,
         dependencies: FullDependencies = DependencyManager.shared) {
        self.window = window
        self.application = application
        self.launchOptions = launchOptions
        self.dependencies = dependencies
    }
    
    func start() {
        // Perform initial application seutp.
        setupAfterLaunch()
        
        // Start your first flow here. For example, this is the
        // ideal place for deciding if you should show login or main flows.
        showLogin()
        
        // Finally make the window key and visible.
        window.makeKeyAndVisible()
    }
    
    // MARK: - Flows -
    
//    func showMain() {
//        // Create your child coordinator here, add it as a child and start it.
//        // Make sure you set the root view controller of the window.
//
//        let coordinator = MainCoordinator(window: window, dependencies: dependencies)
//        children.append(coordinator)
//        coordinator.start()
//    }
    
    // MARK: - Additional Setup -
    
    func setupAfterLaunch() {
        // Perform initial app setup after launch like analytics, integrations and more.
    }
}

private extension AppCoordinator {
    func showLogin() {
        let coordinator = LoginCoordinator(window: window, dependencies: dependencies)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }

    func showProjects(projects: [AppkeeProject]) {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.
        
        let coordinator = ProjectsCoordinator(window: window, dependencies: dependencies, projects: projects)
        coordinator.delegate = self
        children = coordinator
        coordinator.start()
    }
    
    func showProject(code: Int) {
        // Create your child coordinator here, add it as a child and start it.
        // Make sure you set the root view controller of the window.
        
        let coordinator = AppkeeAppCoordinator(window: window, application: application, launchOptions: launchOptions)
        coordinator.delegate = self
        
        appkeeChildren = coordinator
        
        coordinator.start(appCode: String(code), appMenu: true)
    }
}

extension AppCoordinator: CoordinatorDelegate {
    func navigate(to route: CoordinatorRoutes) {
        switch route {
        case .login:
            showLogin()
        case let  .projects(projects):
            self.projects = projects
            showProjects(projects: self.projects)
        case let .openProject(code):
            showProject(code: code)
        }
    }
}

extension AppCoordinator: AppkeeDelegate {
    func close() {
         showProjects(projects: projects)
    }
}

extension AppCoordinator {
    func showNotification(title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "common.ok".localized, style: .default, handler: nil))
        
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
