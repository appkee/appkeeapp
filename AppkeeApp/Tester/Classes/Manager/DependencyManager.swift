//
//  DependenciesManager.swift
//  Core
//
//  Created by Radek Zmeskal on 05/05/2019.
//  Copyright © 2019 Radek Zmeskal. All rights reserved.
//

import Foundation
import AppkeeCoreFramework

typealias FullDependencies =
    HasRepository
    
final class DependencyManager: FullDependencies {
    
    // MARK: - Static Properties
    static let shared = DependencyManager()
    
    // MARK: - Properties
    let repository: AppkeeRepository
    
//    let graphicManager: GraphicManager
    
    // MARK: - Init
    private init() {
        // Setup repository
        let repository = AppkeeServerRepository()
        self.repository = repository                
    }
}
