//
//  ProjectsProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol ProjectsCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol ProjectsCoordinatorInput: class {
    func navigate(to route: Projects.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol ProjectsInteractorInput {
    // func perform(_ request: Projects.Request.Work)
}

// INTERACTOR -> PRESENTER (indirect)
protocol ProjectsInteractorOutput: class {
    // func present(_ response: Projects.Response.Work)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol ProjectsPresenterInput {
    var numberOfSections: Int { get }
    func numberOfItems(in section: Int, isFiltering: Bool) -> Int
    
    func viewCreated()
    func handle(_ action: Projects.Action, isFiltering: Bool)
    
    func configure(_ item: ProjectsTableViewCell, at indexPath: IndexPath, isFiltering: Bool)

    func filter(text: String?)
}

// PRESENTER -> VIEW
protocol ProjectsPresenterOutput: class {
    func display(_ displayModel: Projects.DisplayData.Reload)
}
