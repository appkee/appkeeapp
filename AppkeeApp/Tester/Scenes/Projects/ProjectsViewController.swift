//
//  ProjectsViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController {
    // MARK: - Outlets
    private let tableLabel: UILabel = {
         let label = UILabel()
         label.text = "_No projects"
         label.textAlignment = .center
         label.isHidden = true
         return label
    }()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(ProjectsTableViewCell.nib, forCellReuseIdentifier: ProjectsTableViewCell.reuseId)
            tableView.estimatedRowHeight = 40
//            tableView.separatorStyle = .none
            tableView.backgroundView = tableLabel
        }
    }        
    
    // MARK: - Properties
    private var presenter: ProjectsPresenterInput!

    private let searchController = UISearchController(searchResultsController: nil)

    private var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }

    // MARK: - Init
    class func instantiate(with presenter: ProjectsPresenterInput) -> ProjectsViewController {
        let name = "\(ProjectsViewController.self)"
        let storyboard = UIStoryboard(name: name, bundle: nil)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! ProjectsViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()

        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "menu.search".localized
        searchController.dimsBackgroundDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false

        searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar

        definesPresentationContext = true
    }

    // MARK: - Callbacks -

    
}

extension ProjectsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems(in: section, isFiltering: isFiltering)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProjectsTableViewCell.reuseId, for: indexPath) as! ProjectsTableViewCell
        presenter.configure(cell, at: indexPath, isFiltering: isFiltering)
        return cell
    }
}

extension ProjectsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.handle(.navigate(indexPath.row), isFiltering: isFiltering)
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension ProjectsViewController: ProjectsPresenterOutput {
    func display(_ displayModel: Projects.DisplayData.Reload) {
        tableView.reloadData()
    }
}


extension ProjectsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        presenter.filter(text: searchBar.text)
        self.tableView.reloadData()
    }
}
