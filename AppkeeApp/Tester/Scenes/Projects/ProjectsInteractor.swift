//
//  ProjectsInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

class ProjectsInteractor {
    // MARK: - Properties
    weak var output: ProjectsInteractorOutput?

    // MARK: - Init
    init() {
        
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension ProjectsInteractor: ProjectsInteractorInput {
}
