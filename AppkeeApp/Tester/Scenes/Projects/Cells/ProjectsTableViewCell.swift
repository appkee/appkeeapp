//
//  ProjectsTableViewCell.swift
//  Tester
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import UIKit

protocol AppkeeTableViewCellDelegate: class {
    func configure(with title: String?)
}

class ProjectsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ProjectsTableViewCell: AppkeeTableViewCellDelegate {
    func configure(with title: String?) {
        titleLabel.text = title        
    }
}
