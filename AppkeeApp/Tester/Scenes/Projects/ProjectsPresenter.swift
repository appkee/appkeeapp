//
//  ProjectsPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import AppkeeCoreFramework

class ProjectsPresenter {
    // MARK: - Properties
    let interactor: ProjectsInteractorInput
    weak var coordinator: ProjectsCoordinatorInput?
    weak var output: ProjectsPresenterOutput?
    
    var projects: [AppkeeProject] = []
    var filteredProjects: [AppkeeProject] = []

    // MARK: - Init
    init(interactor: ProjectsInteractorInput, coordinator: ProjectsCoordinatorInput, projects: [AppkeeProject]) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.projects = projects
    }
}

// MARK: - User Events -

extension ProjectsPresenter: ProjectsPresenterInput {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(in section: Int, isFiltering: Bool) -> Int {
        if isFiltering {
            return filteredProjects.count
        }
        return projects.count
    }
    
    func configure(_ item: ProjectsTableViewCell, at indexPath: IndexPath, isFiltering: Bool) {
        if isFiltering {
            let project = filteredProjects[indexPath.row]

            item.configure(with: project.name)
            return
        }

        let project = projects[indexPath.row]
        
        item.configure(with: project.name)
    }
    
    func viewCreated() {
        output?.display(Projects.DisplayData.Reload(emptyArray: projects.isEmpty))
    }

    func filter(text: String?) {
        if let text = text {
            filteredProjects = projects.filter { $0.name.lowercased().contains(text.lowercased()) }
        } else {
            filteredProjects = []
        }
    }

    func handle(_ action: Projects.Action, isFiltering: Bool) {
        switch action {
        case let .navigate(index):
            if isFiltering {
                let project = filteredProjects[index]

                coordinator?.navigate(to: .openProject(project.id))
                return
            }
            let project = projects[index]
            
            coordinator?.navigate(to: .openProject(project.id))
        case .logout:
            UserDefaults.savedEmail = nil
            UserDefaults.savedPassword = nil
            UserDefaults.autologin = false
            
            coordinator?.navigate(to: .logout)
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension ProjectsPresenter: ProjectsInteractorOutput {

}
