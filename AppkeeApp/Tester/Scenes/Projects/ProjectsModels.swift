//
//  ProjectsModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

enum Projects {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case navigate(Int)
        case logout
    }

    enum Route {
        case logout
        case openProject(Int)
    }
}

extension Projects.Request {
    struct Logout {
    }
}

extension Projects.Response {

}

extension Projects.DisplayData {
    struct Reload {
        let emptyArray: Bool
    }
}
