//
//  ProjectsCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit
import AppkeeCoreFramework

class ProjectsCoordinator: Coordinator {
    var dependencies: FullDependencies
        
    // MARK: - Properties
    private let window: UIWindow
    private let navigationController: UINavigationController
    
    var children: Coordinator?
    var projects: [AppkeeProject]
        
    weak var delegate: CoordinatorDelegate?

    // MARK: - Init
    init(window: UIWindow, dependencies: FullDependencies, projects: [AppkeeProject]) {
        self.dependencies = dependencies
        self.window = window
        self.projects = projects
        self.navigationController = UINavigationController()
        self.navigationController.view.backgroundColor = .white
    }
        
    func start() {
        navigationController.navigationBar.barTintColor = AppkeeConstants.appkeeColor
        navigationController.navigationBar.tintColor = .white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController.navigationBar.isHidden = false
        
    
        let interactor = ProjectsInteractor()
        let presenter = ProjectsPresenter(interactor: interactor, coordinator: self, projects: projects)
        let vc = ProjectsViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
        vc.title = "login.apps.title".localized
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "dots_menu"), style: .done, target: self, action: #selector(menu(_:)))
        
        navigationController.setViewControllers([vc], animated: false)        
        
        window.rootViewController = navigationController
    }
    
    @objc func menu(_ barButtonItem: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "login.apps.logout".localized, style: .destructive, handler:{ (UIAlertAction) in
            self.delegate?.navigate(to: .login)
        }))
         
        alert.addAction(UIAlertAction(title: "login.apps.cancel".localized, style: .cancel, handler: nil))
        
        self.navigationController.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}
// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension ProjectsCoordinator: ProjectsCoordinatorInput {
    func navigate(to route: Projects.Route) {
        switch route {
        case .logout:
            delegate?.navigate(to: .login)
        case let .openProject(code):
            delegate?.navigate(to: .openProject(code: code))
        }
    }
}
