//
//  LoginCoordinator.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

class LoginCoordinator: Coordinator {
    var dependencies: FullDependencies
        
    // MARK: - Properties    
    private let window: UIWindow        
    var children: Coordinator?
    
    weak var delegate: CoordinatorDelegate?

    // MARK: - Init
    init(window: UIWindow, dependencies: FullDependencies) {
        self.dependencies = dependencies
        self.window = window
    }

    func start() {
        let interactor = LoginInteractor(repository: dependencies.repository)
        let presenter = LoginPresenter(interactor: interactor, coordinator: self)
        let vc = LoginViewController.instantiate(with: presenter)

        interactor.output = presenter
        presenter.output = vc
        
        window.rootViewController = vc
    }   
}

// MARK: - Navigation Callbacks
// PRESENTER -> COORDINATOR
extension LoginCoordinator: LoginCoordinatorInput {
    func navigate(to route: Login.Route) {
        switch route {
        case .showProjects(let projects):
            delegate?.navigate(to: .projects(projects: projects))
        }
    }
}
