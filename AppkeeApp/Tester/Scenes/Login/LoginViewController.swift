//
//  LoginViewController.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import UIKit
import AppkeeCoreFramework

class LoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.placeholder = "login.email".localized
            
            emailTextField.leftViewMode = .always
            emailTextField.leftView = UIImageView(image: #imageLiteral(resourceName: "pepi_login"))
        }
    }
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.placeholder = "login.password".localized
            
            passwordTextField.leftViewMode = .always
            passwordTextField.leftView = UIImageView(image: #imageLiteral(resourceName: "pepi_password"))
        }
    }
    
    @IBOutlet weak var autologinLabel: UILabel! {
        didSet {
            autologinLabel.text = "login.autologin".localized
        }
    }    
    
    @IBOutlet weak var autologinSwitch: UISwitch! {
        didSet {
            autologinSwitch.onTintColor = AppkeeConstants.appkeeColor
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.backgroundColor = AppkeeConstants.appkeeColor
            loginButton.tintColor = UIColor.white
            loginButton.setTitle("login.loginButton".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.text = "login.description".localized.uppercased()
        }
    }
    
    // MARK: - Properties
    private var presenter: LoginPresenterInput!

    // MARK: - Init
    class func instantiate(with presenter: LoginPresenterInput) -> LoginViewController {
        let name = "\(LoginViewController.self)"
        let storyboard = UIStoryboard(name: name, bundle: nil)
        // swiftlint:disable:next force_cast
        let vc = storyboard.instantiateViewController(withIdentifier: name) as! LoginViewController
        vc.presenter = presenter
        return vc
    }

    // MARK: - View Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewCreated()
    }

    // MARK: - Callbacks -

    @IBAction func autologinSwitchTap(_ sender: Any) {
        
    }
        
    @IBAction func loginButtonTap(_ sender: Any) {
        showProgress(title: "login.logining".localized)
        presenter.handle(.login(email: emailTextField.text, password: passwordTextField.text, autologin: autologinSwitch.isOn))
    }
}

// MARK: - Display Logic -

// PRESENTER -> VIEW
extension LoginViewController: LoginPresenterOutput, Alertable, Progressable {
    func display(_ displayModel: Login.DisplayData.Loading) {
        emailTextField.text = displayModel.email
        passwordTextField.text = displayModel.password
        autologinSwitch.setOn(true, animated: true)
        showProgress(title: "login.logining".localized)
    }
    
    func display(_ displayModel: Login.DisplayData.Logged) {
        hideProgress()
    }
    
    func display(_ displayModel: Login.DisplayData.Error) {
        hideProgress()
        
        showErrorAlert(withMessage: displayModel.message)
    }
}
