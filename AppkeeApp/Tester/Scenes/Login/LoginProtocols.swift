//
//  LoginProtocols.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import UIKit

// ======== Coordinator ======== //

//protocol LoginCoordinatorDelegate: class {
//    func coordinator(_ coordinator: Coordinator, finishedWithSuccess success: Bool)
//}

// PRESENTER -> COORDINATOR
protocol LoginCoordinatorInput: class {
    func navigate(to route: Login.Route)
}

// ======== Interactor ======== //

// PRESENTER -> INTERACTOR
protocol LoginInteractorInput {
    func perform(_ request: Login.Request.Login)
}

// INTERACTOR -> PRESENTER (indirect)
protocol LoginInteractorOutput: class {
    func present(_ response: Login.Response.Projects)
    func present(_ response: Login.Response.Error)
}

// ======== Presenter ======== //

// VIEW -> PRESENTER
protocol LoginPresenterInput {
    func viewCreated()
    func handle(_ action: Login.Action)
}

// PRESENTER -> VIEW
protocol LoginPresenterOutput: class {
    func display(_ displayModel: Login.DisplayData.Error)
    func display(_ displayModel: Login.DisplayData.Logged)
    func display(_ displayModel: Login.DisplayData.Loading)
}
