//
//  LoginPresenter.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import AppkeeCoreFramework

class LoginPresenter {
    // MARK: - Properties
    let interactor: LoginInteractorInput
    weak var coordinator: LoginCoordinatorInput?
    weak var output: LoginPresenterOutput?

    // MARK: - Init
    init(interactor: LoginInteractorInput, coordinator: LoginCoordinatorInput) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
}

// MARK: - User Events -

extension LoginPresenter: LoginPresenterInput {
    func viewCreated() {
        if UserDefaults.autologin {
            if let email = UserDefaults.savedEmail, let password = UserDefaults.savedPassword {
                output?.display(Login.DisplayData.Loading(email: email, password: password))
                interactor.perform(Login.Request.Login(email: email, password: password))
            }
        }
    }

    func handle(_ action: Login.Action) {
        switch action {
        case let .login(email, password, autologin):
            guard let email = email, !email.isEmpty else {
                output?.display(Login.DisplayData.Error(message: "login.error.email"))
                return
            }
            guard let password = password, !password.isEmpty else {
                output?.display(Login.DisplayData.Error(message: "login.error.password"))
                return
            }
            
            if autologin {
                UserDefaults.savedEmail = email
                UserDefaults.savedPassword = password
                UserDefaults.autologin = true
            } else {
                UserDefaults.savedEmail = nil
                UserDefaults.savedPassword = nil
                UserDefaults.autologin = false
            }
            
            interactor.perform(Login.Request.Login(email: email, password: password))
        }
    }
}

// MARK: - Presentation Logic -

// INTERACTOR -> PRESENTER (indirect)
extension LoginPresenter: LoginInteractorOutput {
    func present(_ response: Login.Response.Projects) {
        output?.display(Login.DisplayData.Logged())
        
        coordinator?.navigate(to: Login.Route.showProjects(projects: response.projects))
    }
    
    func present(_ response: Login.Response.Error) {
        output?.display(Login.DisplayData.Error(message: response.message))
    }
}
