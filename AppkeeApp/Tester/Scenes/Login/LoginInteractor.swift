//
//  LoginInteractor.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import AppkeeCoreFramework
import FirebaseAnalytics

class LoginInteractor {
    // MARK: - Properties
    weak var output: LoginInteractorOutput?
    private let repository: AppkeeRepository
    
    // MARK: - Init
    init(repository: AppkeeRepository) {
        self.repository = repository
    }
}

// MARK: - Business Logic -

// PRESENTER -> INTERACTOR
extension LoginInteractor: LoginInteractorInput {
    func perform(_ request: Login.Request.Login) {
        repository.login(email: request.email, password: request.password) { [weak self] response in
            guard let self = self else { return }
            
            switch response.result {
            case .success(let response):
                if response.success {
                    self.output?.present(Login.Response.Projects(projects: response.data.apps))
                } else {
                    self.output?.present(Login.Response.Error(message: response.error ?? "Unknown error"))
                }
            case .failure(let error):
                self.output?.present(Login.Response.Error(message: error.localizedDescription))
                
                print("\(error)")
                Analytics.logEvent("Login to app failed", parameters: ["error": error.localizedDescription])
            }
        }
    }

}
