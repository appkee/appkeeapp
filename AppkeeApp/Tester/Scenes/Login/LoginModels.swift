//
//  LoginModels.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 02/02/2020.
//  Copyright (c) 2020 Radek Zmeskal. All rights reserved.
//

import Foundation
import AppkeeCoreFramework

enum Login {
    enum Request { }
    enum Response { }
    enum DisplayData { }

    enum Action {
        case login(email: String?, password: String?, autologin: Bool)
    }

    enum Route {
        case showProjects(projects: [AppkeeProject])
    }
}

extension Login.Request {
    struct Login {
        let email: String
        let password: String
    }
}

extension Login.Response {
    struct Projects {
        let projects: [AppkeeProject]
    }
    
    struct Error {
        let message: String
    }
}

extension Login.DisplayData {
    struct Error {
        let message: String
    }
    
    struct Loading {
        let email: String
        let password: String
    }
    
    struct Logged {
        
    }
}
