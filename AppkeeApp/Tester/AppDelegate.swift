//
//  AppDelegate.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 27/12/2018.
//  Copyright © 2018 Radek Zmeskal. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import NotificationCenter

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let window = UIWindow(frame: UIScreen.main.bounds)
    var coordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         
        if #available(iOS 13.0, *) {
            window.overrideUserInterfaceStyle = .light
        }
        
//        Fabric.with([Crashlytics.self])
        
        coordinator = AppCoordinator(window: window, application: application, launchOptions: launchOptions)
        coordinator.start()
        
        IQKeyboardManager.shared.enable = true
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    //MARK: - notifications
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
//    }
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//        // If you are receiving a notification message while your app is in the background,
//        // this callback will not be fired till the user taps on the notification launching the application.
//        // TODO: Handle data of notification
//
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//        // Print message ID.
//        if let userInfo = userInfo as? [String: Any] {
//            parseNotification(userInfo: userInfo)
//        }
//
//        // Print full message.
//        print(userInfo)
//    }
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        // If you are receiving a notification message while your app is in the background,
//        // this callback will not be fired till the user taps on the notification launching the application.
//        // TODO: Handle data of notification
//
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//        // Print message ID.
//        if let userInfo = userInfo as? [String: Any] {
//            parseNotification(userInfo: userInfo)
//        }
//
//        // Print full message.
//        print(userInfo)
//
//        completionHandler(UIBackgroundFetchResult.newData)
//    }
    
}
//
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//  // Receive displayed notifications for iOS 10 devices.
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              willPresent notification: UNNotification,
//    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//    let userInfo = notification.request.content.userInfo
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//    // Print message ID.
//    if let userInfo = userInfo as? [String: Any] {
//        parseNotification(userInfo: userInfo)
//    }
//
//    // Print full message.
//    print(userInfo)
//
//    // Change this to your preferred presentation option
//    completionHandler([])
//  }
//
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              didReceive response: UNNotificationResponse,
//                              withCompletionHandler completionHandler: @escaping () -> Void) {
//    let userInfo = response.notification.request.content.userInfo
//    
//    // Print message ID.
//    if let userInfo = userInfo as? [String: Any] {
//        parseNotification(userInfo: userInfo)
//    }
//    
//    // Print full message.
//    print(userInfo)
//
//    completionHandler()
//  }
//}
//
//extension AppDelegate : MessagingDelegate {
//  // refresh_token
////  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
////    print("Firebase registration token: \(fcmToken)")
////
////    let dataDict:[String: String] = ["token": fcmToken]
////    NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
////    // TODO: If necessary send token to application server.
////    // Note: This callback is fired at each app startup and whenever a new token is generated.
////  }
//   
//  // ios_10_data_message
//  // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
//  // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
//  func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//    print("Received data message: \(remoteMessage.appData)")
//  }
//}
//
//
//
//extension AppDelegate {
//    func parseNotification(userInfo: [String: Any]) {
//        if let aps = userInfo["aps"] as? [String: Any] {
//            if let alert = aps["alert"] as? [String: Any] {
//                if let title = alert["title"] as? String, let description = alert["body"] as? String {
//                    coordinator.showNotification(title: title, description: description)
//                }
//            }
//        }
//    }
//}
