//
//  File.swift
//  AppkeeCore
//
//  Created by Radek Zmeskal on 03/02/2020.
//  Copyright © 2020 Radek Zmeskal. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    private enum UserDefsKeys {
        static let email = "UserManager-Key-Login"
        static let password = "UserManager-Key-Password"
        static let autologin = "UserManager-Key-Autologin"        
    }
        
    class var savedEmail: String? {
        get {
            let email = UserDefaults.standard.string(forKey: UserDefsKeys.email)
            return email
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefsKeys.email)
        }
    }
    
    class var savedPassword: String? {
        get {
            let email = UserDefaults.standard.string(forKey: UserDefsKeys.password)
            return email
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefsKeys.password)
        }
    }
    
    class var autologin: Bool {
        get {
            let autologin = UserDefaults.standard.bool(forKey: UserDefsKeys.autologin)
            return autologin
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefsKeys.autologin)
        }
    }
}
